<?php

namespace App\Http\Requests\crowdfunding\profile;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'=>'required|image:jpeg,png,jpg,gif,svg|max:2048',
            'name'=>'regex:/^[\pL\s\-]+$/u|min:6|max:50|unique:users,name|required',
        ];
    }
}
