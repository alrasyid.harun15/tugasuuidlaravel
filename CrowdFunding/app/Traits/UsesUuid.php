<?php
namespace app\Traits;

use Illuminate\Support\Str;

trait UsesUuid{
    
    /**
     * Fungsi ini untuk membantu agar tidak perlu st secara manual UUID
     * di setiap model yang akan kita buat. Suatu tools sederhana
     * yang dapat digunakan, ketika kita penggunakan UUID
     */    
    public static function bootUsesUuid() {
        
        static::creating(function($model){
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
                
    }
    
    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing(){
        
        return false;
        
    }
    
    public function getKeyTypes(){
        
        return "string";
        
    }
    
}

