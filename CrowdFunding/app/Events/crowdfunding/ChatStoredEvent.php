<?php

namespace App\Events\crowdfunding;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\model\chat\Chat;
use App\User;

class ChatStoredEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $chat;    
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Chat $chat)
    {
        $this->chat = $chat;        
    }
    
    public function getChat() {
        return $this->chat;
    }
    
    public function broadcastWith() {
        return[
            /* 'data'=>$this->chat->load(['user']), */
            'chat'=>[
                'subject'=>$this->chat->subject,
                'created_by'=>$this->chat->user->name,
                'created_at'=>$this->chat->created_at->diffForHumans(),
                'is_me'=>"false"
            ]
            
        ];
    }
    
    
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('Crowd-Funding-Chat-C');
    }
}
