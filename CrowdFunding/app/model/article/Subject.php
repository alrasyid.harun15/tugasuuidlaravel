<?php

namespace App\model\article;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Subject extends Model
{
    use UsesUuid;
    
    protected $fillable = ['id','name','slug'];
    
    protected $table = 'subjects';
    
    protected $name = 'sub';
    
    public function getRouteKeyName(){
        return 'name';
    }
    
    public function articles() {
        return $this->hasMany(Article::class);
    }
}
