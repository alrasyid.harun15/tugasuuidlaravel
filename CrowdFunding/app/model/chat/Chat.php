<?php

namespace App\model\chat;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use App\User;

class Chat extends Model
{
    use UsesUuid;
    
    protected $table = 'chats';
    protected $guarded = [];
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
}
