<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class PasswordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>$this->name,
            'email'=>$this->email,
        ];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {
        $name=$this->name;
        $email=$this->email;
        
        return [
            'response_code'=>'00',
            'response_message'=>'Hai '. $name .' dengan Email ' . $email . ' Password anda sudah berhasil disimpan, silahkan login melalui link berikut!' . route("login")
        ];
    }    
    
}
