<?php

namespace App\Http\Resources\crowdfunding;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount'=>$this->amount,
            'reff_id'=>$this->reff_id,
            'status'=>$this->status,
            'method'=>$this->method,
            'naration'=>$this->naration,
            'created_by'=>$this->user->name,
            'created_at'=>$this->created_at,
            'midtrans_response'=>!is_null($this->midtrans_response)?$this->midtrans_response:[]
        ];
    }
    
    public function with($request)
    {
        return [
            'response_code'=>'00',
            'response_message'=>'Transaksi Sukses'
        ];
    }
}
