<?php

namespace App\Http\Middleware\crowdfunding;

use Illuminate\Support\Facades\Auth;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
                        
        if(!is_null($user) && ($user->getRole->name==='Admin')){
            
            return $next($request);
            
        }
        
        abort(403,"Akses kesini terlarang untuk anda!");
        
        
    }
}
