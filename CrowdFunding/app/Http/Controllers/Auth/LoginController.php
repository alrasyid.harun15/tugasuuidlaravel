<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\Auth\LoginResource;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {   
        
        $loginParam = $request->only(['email','password']);
        
        $token = auth()->attempt($loginParam);
        
        if(!$token){
            
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Login gagal!',
            ],401);
            
        }
        
        $user = auth()->user();
        
        return $this->validateUser($user, $token);
        
    }
    
    /**
     * @param user
     */
    private function validateUser($user, $token)
    {
        if(is_null($user)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'User anda tidak ditemukan!'
            ]);
        }
        
        if(is_null($user->email_verified_at)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Email anda ' . $user->email .' belum diverifikasi!'
            ]);
        }
        
        return response()->json([
            'response_code'=>'00',
            'response_message'=>'Login berhasil, Selamat datang : '. auth()->user()->name,
            'data'=>
                    [
                        'token'=>[
                            'access_token' => $token,
                            'token_type' => 'bearer',
                            'expires_in' => auth()->factory()->getTTL() * 60
                        ],'profile'=>[
                            'name' => auth()->user()->name,
                            'email' => auth()->user()->email,
                            'avatar'=> is_null(auth()->user()->image)?'https://randomuser.me/api/portraits/med/lego/5.jpg':route("showImage",['users',auth()->user()->image])
                        ]
                    ]
                
            ]);}

}
