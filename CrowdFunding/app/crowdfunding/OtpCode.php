<?php

namespace App\crowdfunding;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use App\User;

class OtpCode extends Model
{
    use UsesUuid;
    
    protected $table = 'otp_codes';

    protected $guarded = []; 

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
