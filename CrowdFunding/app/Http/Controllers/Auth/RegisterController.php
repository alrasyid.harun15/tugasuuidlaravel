<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Events\OtpGenerate;
use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\Auth\RegisterResource;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {   

        $user = User::create([
            'name'=>$request['name'],
            'email'=>$request['email']            
        ]);
        
        event(new UserRegistered($user));
        
        /*
         *  Tidak diperlukan karena dalam 1 email notifkasi registrasi termasuk OTP
         *  event(new OtpGenerate($user->otpCode)); 
         *  
         *  */
        
        return (new RegisterResource($user))->response()->setStatusCode("200");
        
    }
    
}
