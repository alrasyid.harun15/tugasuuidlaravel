<?php

use Illuminate\Database\Seeder;
use App\model\chat\Chat;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   

        /* factory(App\User::class, 10)->create()->each(function ($user){
            $user->otpCode()->save(factory(App\crowdfunding\OtpCode::class)->make());
        }); */
        
        factory(App\User::class, 10)->create()->each(function ($user){
            $user->chats()->save(factory(Chat::class)->make());
        });;
        
    }
}
