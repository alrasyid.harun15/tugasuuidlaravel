export default{

	namespaced : true,

	state:{
		title:'',
		message:'',
		theErrors:[],
		show:false

	},

	mutations:{

		updateShowProperties(state, alertProps){
			let{title, message, theErrors=[]} = alertProps

			state.title = title
			state.message = message
			state.theErrors = theErrors
			state.show = true
		}

	},

	actions:{
		show(state, alertProps){
			
			state.commit('updateShowProperties',alertProps)

		},
		close(state){
			state.title = ''
			state.message = ''
			state.theErrors = []
			state.show = false
		}
	},

	getters:{

		isShow(state){

			console.log("getters isShow called")

			return state

		},

	}

}