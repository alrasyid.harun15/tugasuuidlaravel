<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Carbon\Carbon;

class ExternalLoginController extends Controller
{
    function redirectToProvider($provider) {
        
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        
        return response()->json([
            'url'=>$url
        ]);
    }
    
    function handleProviderCallback($provider) {
        
        try {
            
            $social_user = Socialite::driver($provider)->stateless()->user();
            
            if(is_null($social_user)){
                return response()->json([
                    'response_code'=>'01',
                    'response_message'=>'Login Social Gagal!',
                ]);
            }
            
            $local_user = User::query()->Where('email','=',$social_user->email)->first();
            
            if(is_null($local_user)){
                
                //$social_user->avatar_original
                
                $local_user = User::create([
                                    'name'=>$social_user->name,
                                    'email'=>$social_user->email,
                                    'email_verified_at'=>now(),
                                    'remember_token'=>$social_user->token,                    
                                ]);
                
            }else if($local_user->email == $social_user->email){
                
                User::query()   ->Where('email','=',$social_user->email)
                                ->update([
                                        'remember_token'=>$social_user->token,
                                        ]);
             
                $local_user = User::query()->Where('email','=',$social_user->email)->first();
                                            
            }
            
            $token = auth()->login($local_user);
            
            return response()->json([
                'response_code'=>'00',
                'response_message'=>'Login berhasil, Selamat datang : '. auth()->user()->name,
                'data'=>
                [
                    'token'=>[
                        'access_token' => $token,
                        'token_type' => 'bearer',
                        'expires_in' => auth()->factory()->getTTL() * 60
                    ],'profile'=>[
                        'name' => auth()->user()->name,
                        'email' => auth()->user()->email,
                        'avatar'=> is_null(auth()->user()->image)?'https://randomuser.me/api/portraits/med/lego/5.jpg':route("showImage",['users',auth()->user()->image])
                    ]
                ]
            ]);
            
        } 
        catch (\Exception $e) {
            
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Login Social Gagal!: '.$e->getMessage(),
            ]);
            
            
        }
        
    }
}