<?php

namespace App\Http\Controllers\crowdfunding;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\chat\Chat;
use App\Http\Requests\crowdfunding\ChatRequest;
use App\Http\Resources\crowdfunding\ChatResources;
use App\User;
use App\Events\crowdfunding\ChatStoredEvent;

class ChatController extends Controller
{
    public function index() {
        
        $chats = Chat::orderBy('created_at','asc')->get();
        
        return ChatResources::collection($chats);
    }
    
    public function send(ChatRequest $request) {
        
        $email = $request['user_email'];
        
        $userFromDB = User::query()->where('email','=',$email)->get()[0];
        
        if($userFromDB->email == $email){
            
            $chat = Chat::create([
                'subject'=>$request['subject'],
                'user_id'=>$userFromDB->id,
            ]);
            
            
            broadcast(new ChatStoredEvent($chat))->toOthers();
            /* event(new ChatStoredEvent($chat)); */
            
            return new ChatResources($chat);
        }else{
            
            return response()->json([
                'response_code'=>'00',
                'response_message'=>'Chat Tidak dapat disimpan!',
            ]);
            
        }
        
        
        
    }
}
