<?php

use Illuminate\Database\Seeder;
use App\model\transaction\Transaction;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Transaction::class,100)->create();
    }
}
