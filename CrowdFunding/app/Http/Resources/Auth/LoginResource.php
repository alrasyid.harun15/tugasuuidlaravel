<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'token' => $this
        ];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {
        return [
            'response_code'=>'00',
            'response_message'=>'Login berhasil!',
        ];
    }
    
}
