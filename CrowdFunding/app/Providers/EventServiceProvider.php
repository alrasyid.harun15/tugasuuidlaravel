<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],'App\Events\UserRegistered' => [
            'App\Listeners\SendRegisteredEmailNotification','App\Listeners\SendRegisteredTelegramNotification',
        ],'App\Events\OtpGenerate' => [
            'App\Listeners\SendOtpGenerateEmailNotification',
        ],'App\Events\OtpRegenerate' => [
            'App\Listeners\SendOtpRegenerateEmailNotification',
        ],'App\Events\ProfileRequestEvent' => [
            'App\Listeners\SendProfileRequestEmailNotification',
        ],'App\Events\VaRequestEvent' => [
            'App\Listeners\SendVaRequestEmailNotification',
        ],
        
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
