

<p>Anda telah terdaftar di Crown Funding NPL Rendah!</P>

<p>Berikut ini adalah permintaan pembuatan kode OTP Ulang! Karena sepertinya anda tidak menerima email OTP kami sebelummnya, atau Otp anda telah kadaluarsa!<p/>

<p>Mohon masukkan kode OTP berikut <strong>{{$otpCode->otp}}</strong> pada aplikasi Crowd Funding di menu Verifikasi Email!<p/>

<p>Jangan berikan kode notifikasi ini ke siapapun!<p/>


