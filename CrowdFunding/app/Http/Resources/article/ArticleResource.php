<?php

namespace App\Http\Resources\article;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'subject' => $this->subject->name,
            'title' => $this->title,
            'body' => $this->body,
            'create by' => $this->user->name
        ];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {
        return ['Status'=>'Success'];
    }
}
