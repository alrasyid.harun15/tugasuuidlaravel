import Vue from 'vue'
import myRouter from './router.js'
import myBeutify from './plugins/vuetify.js'
import myStorage from './storage.js'
import App from './App.vue'
import './bootstrap.js'

const app = new Vue({
    el: '#app',
    router:myRouter,
    vuetify:myBeutify,
    store:myStorage,
    
    data:{
        message:'This is Vue!'
    },
    components:{
        'app':App        
    }
});
