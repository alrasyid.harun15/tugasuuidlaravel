<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\crowdfunding\Role;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {
    return [        
        'name' => ['Admin','User'][rand(0,1)],
        'remark' => $faker->text,        
    ];
});
