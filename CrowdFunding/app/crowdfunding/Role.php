<?php

namespace App\crowdfunding;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use App\User;

class Role extends Model
{
    
    use UsesUuid;
    
    protected $table = 'roles';
    
    protected $guarded = [];

    public function users(){
        return $this->hasMany(User::class);
    }

}
