<?php

namespace App\Listeners;

use App\Events\ProfileRequestEvent;
use App\Http\Requests\crowdfunding\profile\ProfileRequest;
use App\Mail\profileMailBuilder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendProfileRequestEmailNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProfileRequest  $event
     * @return void
     */
    public function handle(ProfileRequestEvent $event)
    {
        $user = $event->getUser();
        Mail::to($user->email)->send(new profileMailBuilder($user));
    }
}
