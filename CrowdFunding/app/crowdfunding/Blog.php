<?php

namespace App\crowdfunding;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Blog extends Model
{
    use UsesUuid;
    
    protected $table = 'blogs';
    protected $guarded = [];
    
}
