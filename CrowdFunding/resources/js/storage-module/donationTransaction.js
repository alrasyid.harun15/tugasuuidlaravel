export default{

	namespaced : true,

	state: {
				
		donations:[],
				
	  },
	  
  	mutations: {

    	store(state, donation) {
			  
			  state.donations.push(donation)
		},
		remove(state, donationIndex){

			state.donations.splice(donationIndex,1)

		},
		clear(state){

			state.donations = []

		}

	},
	actions:{

		donate (state, donation){

			state.commit('store', donation)

		},
		removeDonation (state, donationIndex){

			state.commit('remove', donationIndex)

		},
		clear (state){

			state.commit('clear')

		},
		
	},getters:{

		donationCounter(state){

			return state.donations.length

		},		
		donations(state){

			return state.donations

		},		

	}

}