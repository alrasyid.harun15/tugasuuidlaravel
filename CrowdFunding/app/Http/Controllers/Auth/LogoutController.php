<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $user = auth()->user();
        if(!is_null($user)){
            
            $name = auth()->user()->name;
            auth()->logout();
            
            return response()->json([
                'response_code'=>'00',
                'response_message'=>'Logout berhasil, terima kasih '.$name.' sudah menggunakan API kami'
            ]);
            
        }
        
        return response()->json([
            'response_code'=>'01',
            'response_message'=>'Belum login atau anda tidak terautentifikasi sebelumnya'
        ],401);
        
    }
}
