<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{any?}','app')->where('any','.*');

/* Route::get('/{any?}', function () {
    return view('app');
})->where('any','.*');
 */
/* 
Auth::routes();

 */
/* Route::get('/home', 'HomeController@index')->name('home');


Route::get('/route-1', 'crowdfunding\WelcomeController@indexFirst')->name('route1')->middleware('email');


Route::group(['middleware' => ['email','role']], function () {    
    Route::get('/route-2', 'crowdfunding\WelcomeController@indexSecond')->name('route2');
}); */
