export default{

	namespaced:true,

	state:{
		component:'',
		status:false
	},
	mutations:{
		keep(state, componentAttribute){
			console.log('request to keep component' )
			console.log(componentAttribute)
			let {component, status} = componentAttribute
			state.component = component
			state.status = status
		}
	},
	actions:{
		setComponent(state, componentAttribute){
			state.commit('keep',componentAttribute)
		},
	},
	getters:{
		getStatus(state){
			return state.status
		},
		getComponent(state){
			console.log(state)
			return state.component
		}
	}

}