<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PasswordRequest;
use App\Http\Resources\Auth\PasswordResource;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(PasswordRequest $request)
    {   
        $emailInput = $request['email'];
        $user = User::query()->where('email','=',$emailInput)->first();
        
        if(is_null($user)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Email anda ' . $emailInput .' tidak ditemukan!'
            ]);
        }
        
        if(is_null($user->email_verified_at)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Email anda ' . $user->email .' belum diverifikasi!'
            ]);
        }
            
        User::query()->find($user->id)
            ->update([
                'password'=>Hash::make($request['password']),
            ]);
        
        return new PasswordResource($user);
    }
}
