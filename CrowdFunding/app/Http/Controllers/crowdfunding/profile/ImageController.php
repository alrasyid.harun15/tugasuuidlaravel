<?php

namespace App\Http\Controllers\crowdfunding\profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class ImageController extends Controller
{
    public function show($objectName,$filename) {
        
        $path = storage_path("app\public\\".$objectName."\\".$filename);
        
        //return $path;
        
        if (!File::exists($path)) {
            abort(404, $path);
        }
        
        $file = File::get($path);
        $type = File::mimeType($path);
        
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        
        return $response;
    }
}
