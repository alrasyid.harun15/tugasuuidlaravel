<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\crowdfunding\OtpCode;
use App\User;

class OtpRegenerate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private OtpCode $otpCode;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($otpCode)
    {
        $this->otpCode = $otpCode;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-OtpRegenerate');
    }
    
    public function getOtpCode() {
        return $this->otpCode;
    }
}
