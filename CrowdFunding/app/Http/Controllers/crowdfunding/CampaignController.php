<?php

namespace App\Http\Controllers\crowdfunding;

use App\crowdfunding\Campaign;
use Illuminate\Http\Request;
use App\Http\Requests\crowdfunding\CampaignRequest;
use App\Http\Resources\crowdfunding\CampaignResource;


class CampaignController extends \App\Http\Controllers\Controller
{
    public function index()
    {
              
        $campaigns = Campaign::paginate(6);
        
        return CampaignResource::collection($campaigns);        
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function random($param)
    {   
        if(is_null($param)){

            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Random harus angka dan minimal 2',                
            ],200);

        }else{
                        
            $campaigns = Campaign::inRandomorder()->limit($param)->get();
                        
            return CampaignResource::collection($campaigns);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignRequest $request)
    {
        $image = $request->file('image');
                
        $originalFileName = $image->getClientOriginalName();
        
        $storageFolder = 'campaign';
        
        $image->storeAs($storageFolder, $originalFileName ,'public');
                
        $campaign = Campaign::create([
            'title'=>$request['title'],
            'description'=>$request['description'],
            'image'=>$originalFileName,
        ]);
        
        return new CampaignResource($campaign);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\crowdfunding\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $campaign = Campaign::findOrFail($id);
        
        if(is_null($campaign)){
            
            return response()->json([
                'response_code'=>'01',
                'response_message'=>`Data Campaign yang dicari dengan id : ${$id} tidak ditemukan!`,
            ],200);
            
        }else{
            
            return new CampaignResource($campaign);
            
        }
        
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\crowdfunding\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\crowdfunding\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        //
    }
    
    public function search($queryParam)
    {
        if(is_null($queryParam)){
            
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Pencarian harus memiliki parameter',
            ],200);
            
        }else{
            
            $campaigns = Campaign::query()->where('title','like','%'.$queryParam.'%')->get();
            
            return CampaignResource::collection($campaigns);
        }
        
    }
}
