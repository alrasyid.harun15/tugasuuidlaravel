<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\crowdfunding\OtpCode;
use Faker\Generator as Faker;

$factory->define(OtpCode::class, function (Faker $faker) {
    return [
        'otp'=>mt_rand(100000,999999),
        'valid_until' => new \DateTime('+5 minutes'),
    ];
});
