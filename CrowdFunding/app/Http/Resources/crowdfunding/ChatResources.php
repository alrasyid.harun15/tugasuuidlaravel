<?php

namespace App\Http\Resources\crowdfunding;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'subject'=>$this->subject,
            'created_at'=>$this->created_at->diffForHumans(),
            'created_by'=>$this->user->name,
            'is_me'=>"false"
        ];
    }
    
    public function with($request)
    {
        return [
            'response_code'=>'00',
            'response_message'=>'Pesan diterima dan tersimpan!'
        ];
    }
    
}
