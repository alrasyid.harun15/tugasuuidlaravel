<?php

namespace App\Listeners;

use App\Events\VaRequestEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\VaNotificationMailBuilder;

class SendVaRequestEmailNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VaRequestEvent  $event
     * @return void
     */
    public function handle(VaRequestEvent $event)
    {
        $transaction = $event->getTransaction();
        
        Mail::to($transaction->user->email)->send(new VaNotificationMailBuilder($transaction));
        
        
    }
}
