<?php

namespace App\crowdfunding;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
use App\model\transaction\Transaction;

class Campaign extends Model
{
    use UsesUuid;
    
    protected $table = 'campaigns';
    protected $guarded = [];
    
    public function transactions() {
        
        return $this->belongsToMany(Transaction::class);
        
    }
    
}
