import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const myRoutesMap = [
  	{ 
		path: '/',
		name : 'home',
		alias: '/home', 
		component: ()=> import ('./views/Home.vue'),
		  
 	},
  	{ 
		path: '/donations',
		name : 'donations',		 
		component: ()=> import ('./views/Donations.vue'),
		  
	 },
	{ 
		path: '/campaigns',
		name : 'campaigns',		 
		component: ()=> import ('./views/campaign/Campaigns.vue'),
		  
	 },
	 { 
		path: '/campaign/:campaignId',
		name : 'campaign',		 
		component: ()=> import ('./views/campaign/Campaign.vue'),
		  
	 },
	 { 
		path: '/blogs',
		name : 'blogs',		 
		component: ()=> import ('./views/blog/Blogs.vue'),
		  
	 },
	 { 
		path: '/blog/:blogId',
		name : 'blog',		 
		component: ()=> import ('./views/blog/Blog.vue'),
		  
	 },
	 { 
		path: '/blog/create',
		name : 'blog.create',		 
		component: ()=> import ('./views/blog/Create.vue'),
		  
	 },
	 { 
		path: '*',
		redirect : '/',	  
	 },
	 /* Move to dialog also { 
		path: '/auth/register',
		name : 'auth.register',		 
		component: ()=> import ('./views/auth/Register.vue'),		  
	 }, */
	 { 
		path: '/auth/verification',
		name : 'auth.verification',		 
		component: ()=> import ('./views/auth/Verification.vue'),		  
	 },
	 { 
		path: '/auth/regenerateOtp',
		name : 'auth.regenerateOtp',		 
		component: ()=> import ('./views/auth/RegenerateOtp.vue'),		  
	 },
	 /* moved to dialog and becomes component{ 
		path: '/login',
		name : 'auth.login',		 
		component: ()=> import ('./views/auth/Login.vue'),		  
	 }, */
	 { 
		path: '/auth/changePassword',
		name : 'auth.changePassword',		 
		component: ()=> import ('./views/auth/ChangePassword.vue'),		  
	 },
	 { 
		path: '/cf/profile/edit',
		name : 'cf.profile.edit',		 
		component: ()=> import ('./views/cf/profile/ViewProfile.vue'),		  
	 },
	 { 
		path: '/auth/login/social/:provider/callback',
		name : 'auth.social.callback',		 
		component: ()=> import ('./views/auth/SocialCallback.vue'),		  
	 },
	 { 
		path: '/chat',
		name : 'chat',		 
		component: ()=> import ('./components/chat/ChatRoom.vue'),		  
	 },
]

export default new VueRouter({
	mode: 'history',
	routes : myRoutesMap

}) 