<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_transaction', function (Blueprint $table) {
            $table->uuid('transaction_id');
            $table->uuid('campaign_id');
            $table->integer('amount')->nullable();     
            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('campaign_id')->references('id')->on('campaigns');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(Blueprint $table)
    {   
        Schema::table('transaction_campaign', function (Blueprint $table) {
            
            $table->dropForeign(['transaction_id']);
            $table->dropForeign(['campaign_id']);
            $table->dropColumn(['transaction_id']);
            $table->dropColumn(['campaign_id']);            
        });
        
        Schema::dropIfExists('transaction_campaign');
    }
}

