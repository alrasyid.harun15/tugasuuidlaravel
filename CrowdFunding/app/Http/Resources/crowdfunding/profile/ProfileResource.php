<?php

namespace App\Http\Resources\crowdfunding\profile;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        
        $imagePath=NULL;
        
        if(!is_null($this->image)){            
            $imagePath = route("showImage",['users',$this->image]);
        }
        
        return ['profile'=>[
            'full_name'=>$this->name,
            'email'=>$this->email,
            'image'=>$imagePath,
            'email_verification'=>$this->email_verified_at->diffForHumans(),
            'role'=>$this->role->name
        ]];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {
        
        return [
            'response_code'=>'00',
            'response_message'=>'Data Profil user berhasil diakses!',
        ];
    }
    
        
}
