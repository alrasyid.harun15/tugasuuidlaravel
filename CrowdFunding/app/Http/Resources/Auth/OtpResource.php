<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class OtpResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>$this->user->name,
            'email'=>$this->user->email                
        ];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {   
        $name=$this->user->name;
        $email=$this->user->email;
        
        return [
            'response_code'=>'00',
            'response_message'=>'Hai '. $name .', Email ' . $email . ' berhasil diverifikasi, silahkan lanjut ke langkah selanjutnya, yaitu membuat password!',
        ];
    }
    
}
