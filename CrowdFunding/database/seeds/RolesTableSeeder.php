<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {           
        /* factory(App\crowdfunding\Role::class, 5)->create()->each(function ($role) {
            $role->Users()->save(factory(App\User::class)->make());
        }); */
        
        factory(App\crowdfunding\Role::class, 5)->create();
    }
}
