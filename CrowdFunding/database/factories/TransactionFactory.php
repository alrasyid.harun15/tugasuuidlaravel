<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\model\transaction\Transaction;
use Faker\Generator as Faker;
use App\User;
use App\crowdfunding\Campaign;

$factory->define(Transaction::class, function (Faker $faker) {
    
    return [
        'amount'=>$faker->numberBetween(1500000,10000000),
        'reff_id'=>$faker->randomNumber(6),
        'status'=>'pending',
        'method'=>'online',
        'naration'=>$faker->text(),
        'user_id'=>(User::inRandomorder()->limit(1)->get()[0])->id,
    ];
});
