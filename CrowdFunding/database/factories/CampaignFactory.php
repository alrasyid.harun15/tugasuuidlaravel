<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\crowdfunding\Campaign;
use Faker\Generator as Faker;

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'title'=>$faker->text(50),
        'description'=>$faker->text(250),
        'image'=>'image'.rand(1, 11).'.jpg' ,
        'required'=>$faker->numberBetween(9500000,10000000),
        'collected'=>$faker->numberBetween(500000,9500000),
        'address'=>$faker->address()
    ];
});
