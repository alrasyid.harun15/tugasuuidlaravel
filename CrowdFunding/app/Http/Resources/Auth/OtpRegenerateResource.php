<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class OtpRegenerateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email'=>$this->user->email
        ];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {
        $name=$this->user->name;
        $email=$this->user->email;
        
        return [
            'response_code'=>'00',
            'response_message'=>'Hai '. $name .' dengan Email ' . $email . ' Mohon check email anda kembali, dan kali ini jangan sampai kelamaan verifikasi emailnya yah!',
        ];
    }
}
