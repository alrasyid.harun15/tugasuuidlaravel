<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\crowdfunding\OtpCode;

class OtpMailBuilder extends Mailable
{
    use Queueable, SerializesModels;
    public $otpCode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otp)
    {
        $this->otpCode = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        return $this->from('no-reply@edgerun.com')
                    ->subject("Notifikasi Verifikasi Email!")
                    ->view('auth.mail.generateOtpMail');
    }
}
