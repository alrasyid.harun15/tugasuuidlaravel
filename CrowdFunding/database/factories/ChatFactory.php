<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\model\chat\Chat;
use Faker\Generator as Faker;

$factory->define(Chat::class, function (Faker $faker) {
    return [
        'subject'=>$faker->text() 
    ];
});
