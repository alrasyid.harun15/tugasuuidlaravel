<?php

namespace App\model\article;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Article extends Model
{
    use UsesUuid;
    
    protected $fillable = ['id','title','slug', 'body', 'user_id', 'subject_id'];
    
    protected $table = 'articles';
    
    protected $name = 'sub';
    
    public function getRouteKeyName(){
        return 'id';
    }
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function subject() {
        return $this->belongsTo(Subject::class);
    }

}
