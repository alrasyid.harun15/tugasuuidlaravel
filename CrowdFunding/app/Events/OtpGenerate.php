<?php

namespace App\Events;

use App\crowdfunding\OtpCode;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OtpGenerate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    private $otpCode;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpCode)
    {
        $this->otpCode = $otpCode;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-OtpGenerate');
    }
    
    public function getOtpCode():OtpCode {
        return $this->otpCode;
    }
}
