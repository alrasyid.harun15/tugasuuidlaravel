<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\crowdfunding\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title'=>$faker->text(50),
        'description'=>$faker->text(250),
        'image'=>'image'.rand(1, 11).'.jpg' 
    ];
});
