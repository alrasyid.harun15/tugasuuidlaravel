(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/auth/ChangePassword.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/auth/ChangePassword.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      valid: true,
      email: '',
      emailRules: [function (v) {
        return !!v || 'E-mail is required';
      }, function (v) {
        return /.+@.+\..+/.test(v) || 'E-mail must be valid';
      }],
      password: '',
      show1: false,
      passwordRules: [function (v) {
        return !!v || 'Password is required';
      }, function (v) {
        return v && v.length > 5 || 'Password Must Be Valid!';
      }],
      password_confirmation: '',
      show2: false,
      message: '',
      theErrors: [],
      snackbar: false,
      vertical: true,
      color: 'purple'
    };
  },
  methods: {
    submit: function submit() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, _response$data, response_message, response_code, _e$response$data, e_message, errors;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return axios.post('/api/auth/update-password', _this.submitPassword());

              case 3:
                response = _context.sent;

                if (response.status == 200) {
                  _response$data = response.data, response_message = _response$data.response_message, response_code = _response$data.response_code;
                  _this.message = response_message;
                  _this.snackbar = true;

                  if (response_code == "00") {
                    _this.color = 'green';

                    _this.$refs.form.reset();
                  } else {
                    _this.color = 'red';
                  }
                }

                _context.next = 19;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0.response);
                _e$response$data = _context.t0.response.data, e_message = _e$response$data.e_message, errors = _e$response$data.errors;
                _this.theErrors = errors;
                _this.success = errors;
                console.log('Error is comming');
                console.log(errors);
                console.log(errors);
                _this.message = e_message;
                _this.snackbar = true;
                _this.color = 'red';

              case 19:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 7]]);
      }))();
    },
    validate: function validate() {
      var isSuccessfullyValidated = this.$refs.form.validate();

      if (isSuccessfullyValidated) {
        console.log("the input parameter is: ".concat(this.email));
        this.submit();
      } else {
        console.log("the problematic input parameter is : ".concat(this.email));
        this.message = 'Your input is not valid!';
        this.snackbar = true;
        this.color = 'red';
      }
    },
    reset: function reset() {
      this.$refs.form.reset();
      this.$refs.form.resetValidation();
    },
    submitPassword: function submitPassword() {
      return {
        email: this.email,
        password: this.password,
        password_confirmation: this.password_confirmation
      };
    }
  },
  mounted: function mounted() {
    console.log('Regenerate OTP Screen Mounted');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/auth/ChangePassword.vue?vue&type=template&id=75dcd3a3&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/auth/ChangePassword.vue?vue&type=template&id=75dcd3a3& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    [
      _c("v-card-title", [_vm._v("\n\t\t\tGanti Password\n\t\t")]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-form",
            {
              ref: "form",
              attrs: { "lazy-validation": "" },
              model: {
                value: _vm.valid,
                callback: function($$v) {
                  _vm.valid = $$v
                },
                expression: "valid"
              }
            },
            [
              _c("v-text-field", {
                attrs: { rules: _vm.emailRules, label: "E-mail", required: "" },
                model: {
                  value: _vm.email,
                  callback: function($$v) {
                    _vm.email = $$v
                  },
                  expression: "email"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  rules: _vm.passwordRules,
                  "append-icon": _vm.show1 ? "mdi-eye" : "mdi-eye-off",
                  type: _vm.show1 ? "text" : "password",
                  label: "Password",
                  required: ""
                },
                on: {
                  "click:append": function($event) {
                    _vm.show1 = !_vm.show1
                  }
                },
                model: {
                  value: _vm.password,
                  callback: function($$v) {
                    _vm.password = $$v
                  },
                  expression: "password"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  rules: _vm.passwordRules,
                  "append-icon": _vm.show2 ? "mdi-eye" : "mdi-eye-off",
                  type: _vm.show2 ? "text" : "password",
                  label: "Password Confirmation",
                  required: ""
                },
                on: {
                  "click:append": function($event) {
                    _vm.show2 = !_vm.show2
                  }
                },
                model: {
                  value: _vm.password_confirmation,
                  callback: function($$v) {
                    _vm.password_confirmation = $$v
                  },
                  expression: "password_confirmation"
                }
              }),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  staticClass: "mr-4",
                  attrs: { disabled: !_vm.valid, color: "success" },
                  on: { click: _vm.validate }
                },
                [_vm._v("\n\t\t\t\t\tSubmit Password\n\t\t\t\t")]
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  staticClass: "mr-4",
                  attrs: { color: "error" },
                  on: { click: _vm.reset }
                },
                [_vm._v("\n\t\t\t\t\tReset\n\t\t\t\t")]
              ),
              _vm._v(" "),
              _c(
                "v-snackbar",
                {
                  attrs: { vertical: _vm.vertical, color: _vm.color },
                  scopedSlots: _vm._u([
                    {
                      key: "action",
                      fn: function(ref) {
                        var attrs = ref.attrs
                        return [
                          _c(
                            "v-btn",
                            _vm._b(
                              {
                                attrs: { color: "indigo", text: "" },
                                on: {
                                  click: function($event) {
                                    _vm.snackbar = false
                                  }
                                }
                              },
                              "v-btn",
                              attrs,
                              false
                            ),
                            [_vm._v("\n\t\t\t\t\t\t\tClose\n\t\t\t\t\t\t")]
                          )
                        ]
                      }
                    }
                  ]),
                  model: {
                    value: _vm.snackbar,
                    callback: function($$v) {
                      _vm.snackbar = $$v
                    },
                    expression: "snackbar"
                  }
                },
                [
                  _vm._v(
                    "\n\t\t\t\t\n\t\t\t\t\t" +
                      _vm._s(_vm.message) +
                      "\n\n\t\t\t\t\t"
                  ),
                  _c(
                    "ul",
                    _vm._l(_vm.theErrors, function(value, key) {
                      return _c("li", { key: "error-at-" + key }, [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t" +
                            _vm._s(key) +
                            " : " +
                            _vm._s(value) +
                            "\n\t\t\t\t\t\t"
                        )
                      ])
                    }),
                    0
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/auth/ChangePassword.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/auth/ChangePassword.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ChangePassword_vue_vue_type_template_id_75dcd3a3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ChangePassword.vue?vue&type=template&id=75dcd3a3& */ "./resources/js/views/auth/ChangePassword.vue?vue&type=template&id=75dcd3a3&");
/* harmony import */ var _ChangePassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChangePassword.vue?vue&type=script&lang=js& */ "./resources/js/views/auth/ChangePassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ChangePassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ChangePassword_vue_vue_type_template_id_75dcd3a3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ChangePassword_vue_vue_type_template_id_75dcd3a3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/auth/ChangePassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/auth/ChangePassword.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/auth/ChangePassword.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangePassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ChangePassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/auth/ChangePassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangePassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/auth/ChangePassword.vue?vue&type=template&id=75dcd3a3&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/auth/ChangePassword.vue?vue&type=template&id=75dcd3a3& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangePassword_vue_vue_type_template_id_75dcd3a3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ChangePassword.vue?vue&type=template&id=75dcd3a3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/auth/ChangePassword.vue?vue&type=template&id=75dcd3a3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangePassword_vue_vue_type_template_id_75dcd3a3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ChangePassword_vue_vue_type_template_id_75dcd3a3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);