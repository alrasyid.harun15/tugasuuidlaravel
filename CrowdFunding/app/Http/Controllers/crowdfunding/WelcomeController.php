<?php

namespace App\Http\Controllers\crowdfunding;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function indexFirst(Request $request){
        
        $name = $request->user()->name;
        
        return "Selamat Datang {$name} di Crowdfunding bebas NPL!";
        
    }
    
    public function indexSecond(Request $request){
        
        $name = $request->user()->name;
        $roleName = $request->user()->getRole->name;
        
        return "Hai {$name} : {$roleName} ... Selamat Datang di Crowdfunding bebas NPL!";
        
    }
}
