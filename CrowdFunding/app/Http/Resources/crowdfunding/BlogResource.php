<?php

namespace App\Http\Resources\crowdfunding;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'image'=>route('showImage',['blog',$this->image])
        ];
    }
    
    /**
     * {@inheritDoc}
     * @see \Illuminate\Http\Resources\Json\JsonResource::with()
     */
    public function with($request)
    {
        
        return [
            'response_code'=>'00',
            'response_message'=>'Data Blog',
        ];
    }
    }
