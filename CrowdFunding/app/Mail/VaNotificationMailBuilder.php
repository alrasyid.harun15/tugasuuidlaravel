<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\model\transaction\Transaction;

class VaNotificationMailBuilder extends Mailable
{
    use Queueable, SerializesModels;
    
    public $transaction;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@edgerun.com')
                    ->subject("Notifikasi VA Donasi")
                    ->view('donation.mail.vaNotification');
    }
}
