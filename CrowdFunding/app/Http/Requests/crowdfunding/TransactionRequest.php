<?php

namespace App\Http\Requests\crowdfunding;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        
        return [
            'amount'=>'required|numeric',
            'method'=>'required|min:3',
            'naration'=>'max:500',
            'campaigns'=>'required|min:10'
        ];
    }
}
