<?php

namespace App\Listeners;

use App\Events\OtpGenerate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\OtpMailBuilder;

class SendOtpGenerateEmailNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpGenerate  $event
     * @return void
     */
    public function handle(OtpGenerate $event)
    {   
        $otpCode = $event->getOtpCode();
        
        Mail::to($otpCode->user->email)->send(new OtpMailBuilder($otpCode));
    }
}
