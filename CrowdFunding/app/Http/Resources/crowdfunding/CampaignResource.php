<?php

namespace App\Http\Resources\crowdfunding;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'description'=>$this->description,
            'address'=>$this->address,
            'required'=>$this->required,
            'collected'=>$this->collected,
            'image'=>route('showImage',['campaign',$this->image]),
        ];
    }
    
    public function with($request)
    {
        return [
            'response_code'=>'00',
            'response_message'=>'Berikut data Campaign hasil random',
        ];
    }
}
