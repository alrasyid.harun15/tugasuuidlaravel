<?php

namespace App\Http\Middleware\crowdfunding;

use Illuminate\Support\Facades\Auth;
use Closure;

class CheckValidatedEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        
        if(!is_null($user) && !is_null($user->email_verified_at)){
            
            return $next($request);
            
        }
    
        abort(403,"Mohon login & verifikasi email anda dahulu!");
        
    }
}
