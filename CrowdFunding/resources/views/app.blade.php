<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Crowd Funding</title>		
	<link rel="stylesheet" href="{{asset( '/css/app.css' )}}">
</head>

<body>
	<main id="app">
		<app></app>
	</main>
</body>
<script src="{{asset( '/js/app.js' )}}"></script>
</html>