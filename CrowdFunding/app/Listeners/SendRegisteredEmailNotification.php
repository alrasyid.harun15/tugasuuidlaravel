<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\RegisteredUserMailBuilder;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRegisteredEmailNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $user = $event->getUser();
        
        Mail::to($user->email)->send(new RegisteredUserMailBuilder($user));
        
    }
}
