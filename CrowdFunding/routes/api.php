<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

/* Route::namespace('Auth')->group(function () {
    
    Route::post('auth/register','RegisterController');
    Route::post('auth/verification','OtpController@verification')->name('verification');
    Route::post('auth/regenerate-otp','OtpController@regenerate')->name('regenerate-otp');
    Route::post('auth/update-password','PasswordController')->name('create-password');
    Route::post('auth/login','LoginController')->name('login');
    Route::post('auth/logout','LogoutController');
    
}); */

Route::group([  'namespace'=>'Auth',    //  class dalam folder auth (name space Auth)
                'prefix'=>'auth',       //  API dengan prefix
                'middleware'=>'api'     //  middleware yang mem-filter
            ],function () {
        
        Route::post('register','RegisterController');
        Route::post('verification','OtpController@verification')->name('verification');
        Route::post('regenerate-otp','OtpController@regenerate')->name('regenerate-otp');
        Route::post('update-password','PasswordController')->name('create-password');
        Route::post('login','LoginController')->name('login');
        Route::post('logout','LogoutController');
        
        Route::get('login/social/{provider}','ExternalLoginController@redirectToProvider');
        Route::get('login/social/{provider}/callback','ExternalLoginController@handleProviderCallback');
});

Route::post('auth/checkToken','Auth\CheckTokenController')->middleware('auth:api');

Route::namespace('crowdfunding\profile')->middleware('auth:api')->group(function () {
    
    Route::get('profile/get-profile','ProfileController@show');
    Route::post('profile/update-profile','ProfileController@update');
    Route::get('profile/sendMail', 'ProfileController@sendMail')->name('sendMail');
    Route::get('storage/{filename}', 'ImageController@show')->name('showImage');
});

Route::namespace('crowdfunding')->middleware('auth:api')->group(function () {
    
    Route::get('chat','ChatController@index');    
    Route::post('chat/send','ChatController@send');    
    
});

Route::group([  'namespace'=>'crowdfunding',
                'prefix'=>'campaign',
                'middleware'=>'api'],function () {
    
    Route::get('{param}/random','CampaignController@random');
    Route::get('{id}/show','CampaignController@show');
    Route::post('store','CampaignController@store');
    Route::get('/','CampaignController@index');
    Route::get('{param}/search','CampaignController@search');
                        
});

Route::group([  'namespace'=>'crowdfunding',
    'prefix'=>'transaction',
    'middleware'=>'auth:api'],function () {
    
    Route::get('{id}/transaction/list','TransactionController@listBy');
    Route::get('{id}/transaction/show','TransactionController@show');
    Route::post('store/transaction','TransactionController@store');
    Route::get('{id}/transaction/update/transaction/status','TransactionController@updateTransactionStatus');
    
    
    });

Route::group([  'namespace'=>'crowdfunding',
    'prefix'=>'blog',
    'middleware'=>'api'],function () {
    
    Route::get('{random}/random','BlogController@random');
    Route::get('{id}/show','BlogController@show');
    Route::post('store','BlogController@store');
    Route::get('/','BlogController@index');
    
    });



Route::get('showImage/{objectname}/{filename}', 'crowdfunding\profile\ImageController@show')->name('showImage')->middleware('api');

































Route::namespace('crowdfunding')->middleware('auth:api')->group(function () {
        
    Route::post('route-1','WelcomeController@indexFirst');
    Route::post('route-2','WelcomeController@indexSecond');
        
});

Route::namespace('article')->middleware('auth:api')->group(function () {
    
    Route::post('article/store','ArticleController@store');
    Route::post('subject/store','SubjectController@store');
        
});
    

Route::namespace('article')->group(function () {
    
    Route::get('article/{article}','ArticleController@show');
    Route::get('subject/{subject}','SubjectController@show');
    
});




