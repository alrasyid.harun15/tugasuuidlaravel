<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\crowdfunding\Campaign;
use App\model\transaction\Transaction;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Campaign::class,20)->create()->each(function($campaign){
            $campaign->transactions()->save(
                    Transaction::inRandomorder()->limit(1)->get()[0]
                );
        });
    }
}
