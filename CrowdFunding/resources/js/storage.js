import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import donationTx from './storage-module/donationTransaction.js'
import auth from './storage-module/authentication.js'
import alert from './storage-module/alert.js'
import component from './storage-module/component.js'

Vue.use(Vuex)

const vuexLocalPersistence = new VuexPersistence({
	
	storage: window.localStorage,
	key: '9765DBAC21255FDB41D939A88B22E-9722C8A967117A2D'
	
})

export default new Vuex.Store({

	plugins:[vuexLocalPersistence.plugin],
  	modules:{
		donationTransaction:donationTx,
		authenticate:auth,
		alert:alert,
		component:component,
	}

})
