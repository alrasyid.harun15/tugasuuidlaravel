<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\OtpRegenerateRequest;
use App\Http\Requests\Auth\OtpRequest;
use App\Http\Resources\Auth\OtpRegenerateResource;
use App\Http\Resources\Auth\OtpResource;
use App\crowdfunding\OtpCode;
use App\User;
use phpDocumentor\Reflection\Types\Null_;
use App\Events\OtpGenerate;
use App\Events\OtpRegenerate;

class OtpController extends Controller
{
    public function verification(OtpRequest $request) {
        
        $otpInput = $request['otp'];
        $otp = OtpCode::query()->where('otp','=',$otpInput)->first();
        
        $businessValidationResult = $this->validateRequestByData($otp);
        
        if(!is_null($businessValidationResult))
            return $businessValidationResult;
        
        $validUntil = $otp->valid_until;
        
        if($validUntil < now()){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Otp Sudah kadaluarsa!, silahkan minta OTP baru lagi!'
            ]);
        }
        
        User::query()->find($otp->user->id)
                     ->update([
                      'email_verified_at'=>now(),
                       ]);
        
        return new OtpResource($otp);
    }
    
    private function validateRequestByData($otp)
    {
        
        if(is_null($otp)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'OTP anda tidak ditemukan!'
            ]);
        }
        
        
        $emailIsVerifiedAt = $otp->user->email_verified_at;
        
        if(!is_null($emailIsVerifiedAt)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Email anda ' . $otp->user->email .' sudah diverifikasi pada '. $otp->user->email_verified_at->diffForHumans() .'!'
            ]);
        }
        
        return Null;
    }
  
    
    public function regenerate(OtpRegenerateRequest $request) {
        
        $emailInput = $request['email'];
        $user = User::query()->where('email','=',$emailInput)->first();
        
        if(is_null($user)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'email anda '. $emailInput .' tidak ditemukan!'
            ]);
        }
        
        $businessValidationResult = $this->validateRequestByData($user->otpCode);
        
        if(!is_null($businessValidationResult))
            return $businessValidationResult;
        
        OtpCode::query()->where("id","=",$user->otpCode->id)
                        ->update([
                            'otp'=>mt_rand(100000,999999),
                            'valid_until'=> new \DateTime('+5 minutes')
                        ]); 
                        
       $user->generateOtpCode();
        
        $UpdatedOtp = OtpCode::find($user->otpCode->id);
                        
        event(new OtpRegenerate($UpdatedOtp));                
                        
        return new OtpRegenerateResource($user->otpCode);
    }  
}
