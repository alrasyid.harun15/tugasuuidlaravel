<?php

namespace App\Http\Requests\crowdfunding;

use Illuminate\Foundation\Http\FormRequest;

class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:5|max:200',
            'description'=>'min:5|max:600',
            'image'=>'image:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }
}
