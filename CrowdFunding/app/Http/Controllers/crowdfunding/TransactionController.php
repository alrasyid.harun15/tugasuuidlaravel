<?php

namespace App\Http\Controllers\crowdfunding;

use App\Http\Controllers\Controller;
use App\Http\Requests\crowdfunding\TransactionRequest;
use App\Http\Resources\crowdfunding\TransactionResources;
use App\model\transaction\Transaction;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use App\Events\VaRequestEvent;
use App\crowdfunding\Campaign;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listBy($userId)
    {   
        $transactions = Transaction::query()->where('user_id','=',$userId)->get();
        
        return TransactionResources::collection($transactions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {   
        
        $loggedInUser = auth()->user();
        
        if(!is_null($loggedInUser)){
            
            $transaction = Transaction::create([
                'amount'=>$request['amount'],
                'status'=>'pending',
                'method'=>$request['method'],
                'naration'=>$request['naration'],
                'user_id'=>$loggedInUser->id,
            ]);
            
            $reffId = $transaction->id . '-' . Str::random(5);
            $transaction->reff_id = $reffId;
            
            $transaction->save();
                         
            $midtransResponse = $this->storeAtMidtrans($transaction);
            
            if(count($midtransResponse) == 0){
                
                $transaction->status = 'failed';
                $transaction->save();
                
                return response()->json([
                    'response_code'=>'01',
                    'response_status'=>'Kegagalan di Mindtrans!',
                ]);
                
            }else{
                
                $transaction->midtrans_transaction_id = $midtransResponse['transaction_id'];
                $transaction->midtrans_bank = $midtransResponse['bank'];
                $transaction->midtrans_va_number = $midtransResponse['va_number'];
                $transaction->save();
                
                $campaigns = json_decode($request['campaigns']);
                
                foreach ($campaigns as $cmp) {
                    $amount = $cmp->amount;
                    $campaign_id = $cmp->campaign->id;
                    
                    $campaign = Campaign::find($campaign_id);
                    $campaign->transactions()->attach($transaction->id, ['amount' => (int)$amount, 'created_at'=>new \DateTime()]);
                }
            }
            
            $transaction->midtrans_response = $midtransResponse;
            
            event(new VaRequestEvent($transaction));
                        
            return new TransactionResources($transaction);
            
        }else{
         
            return response()->json([
                'response_code'=>'01',
                'response_status'=>'Pelaku transaksi tidak terverifikasi!',
            ]);
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($reff_id)
    {
        $transaction = Transaction::query()->where('reff_id','=',$reff_id)->get()[0];
        
        return new TransactionResources($transaction);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTransactionStatus(TransactionRequest $request, $id)
    {
        $success = Transaction::query()->where('id','=',$id)->update([
            /* 'amount'=>$request['amount'], */
            /* 'reff_id'=>$request['reff_id'], */
            'status'=>$request['status'],
            /* 'method'=>$request['method'], */
            'naration'=>$request['naration'],
            /* 'user_id'=>$request['user_id'], */
        ]);
        
        if($success){
            $transaction = Transaction::query()->find($id);
            
            if($transaction->status === $request['status']){
                
                return response()->json([
                    'response_code'=>'00',
                    'response_status'=>'Update status transaksi berhasil',
                ]);
                
            }else{
                
                return response()->json([
                    'response_code'=>'01',
                    'response_status'=>'Update status transaksi gagal',
                ]);
                
            }
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    protected function storeAtMidtrans(Transaction $transaction) {
        
        $server_key = base64_encode(config('app.midtrans.server_key'));
        $midtrans_server_URI = config('app.midtrans.base_uri');
        $midtrans_version_charge = '/v2/charge';
        
        $client = new Client([
            'base_uri' => $midtrans_server_URI
        ]);
        
        $headers = [            
                'Accept'=>'application/json',
                'Authorization'=>'Basic '.$server_key,
                'Content-Type'=>'application/json'            
        ];
        
        $body = $this->constructMessageBody($transaction);
        
        $response_from_midtrans = $client->post($midtrans_version_charge,[
            'headers'=>$headers,
            'body'=>json_encode($body)
        ]);
        
        $responseInJson = json_decode($response_from_midtrans->getBody());
                
        return $this->readResponseBody($transaction, $responseInJson);
    }
    
    
    private function constructMessageBody(Transaction $transaction)
    {   
        $body = [];
        
        switch ($transaction->method){
            
            case 'bca' : 
                $body = [
                    'payment_type'=>'bank_transfer',
                    'transaction_details'=>[
                        "order_id"=> $transaction->reff_id,
                        "gross_amount"=> $transaction->amount                        
                    ],
                    'bank_transfer'=>[
                            "bank"=> "bca"
                        ]
                ];
                break;
            case 'bni' :
                $body = [
                    'payment_type'=>'bank_transfer',
                    'transaction_details'=>[
                        "order_id"=> $transaction->reff_id,
                        "gross_amount"=> $transaction->amount
                    ],
                    'bank_transfer'=>[
                            "bank"=> "bni"
                        ]
                    ];
                break;
            case 'bri' :
                $body = [
                    'payment_type'=>'bank_transfer',
                    'transaction_details'=>[
                        "order_id"=> $transaction->reff_id,
                        "gross_amount"=> $transaction->amount
                    ],
                    'bank_transfer'=>[
                        "bank"=> "bri"
                        ]
                    ];
                break;
            case 'mandiri_bill' :
                $body = [
                    'payment_type'=>'echannel',
                    'transaction_details'=>[
                        "order_id"=> $transaction->reff_id,
                        "gross_amount"=> $transaction->amount
                        ]
                    ];
                break;
            case 'permata' :
                $body = [
                    'payment_type'=>'permata',
                    'transaction_details'=>[
                        "order_id"=> $transaction->reff_id,
                        "gross_amount"=> $transaction->amount
                    ]
                ];
                break;
        }
        
        return $body;
    }
    
    private function readResponseBody(Transaction $transaction, $response)
    {   
        $body = [];
        
        if($response->status_code=="201"){
            
            switch ($transaction->method){
                
                case 'bca' :
                    $body = [
                    
                        "transaction_id"=>$response->transaction_id,
                        "bank"=>$response->va_numbers[0]->bank,
                        "va_number"=>$response->va_numbers[0]->va_number
                    
                    ];
                    break;
                case 'bni' :
                    $body = [
                    
                        "transaction_id"=>$response->transaction_id,
                        "bank"=>$response->va_numbers[0]->bank,
                        "va_number"=>$response->va_numbers[0]->va_number
                    
                    ];
                    break;
                case 'bri' :
                    $body = [
                    
                        "transaction_id"=>$response->transaction_id,
                        "bank"=>$response->va_numbers[0]->bank,
                        "va_number"=>$response->va_numbers[0]->va_number
                    
                    ];
                    break;
                case 'mandiri_bill' :
                    $body = [
                    
                        "transaction_id"=>$response->transaction_id,
                        "bank"=>'mandiri',
                        "bill_key"=>$response->bill_key,
                        "bill_code"=>$response->bill_code
                    
                    ];
                    break;
                case 'permata' :
                    $body = [
                    
                        "transaction_id"=>$response->transaction_id,
                        "bank"=>'permata',
                        "va_number"=>$response->permata_va_number
                    
                    ];
                    break;
            }
            
        }
        
        return $body;
    }

    
}
