<?php

namespace App\Http\Controllers\crowdfunding;

use App\crowdfunding\Blog;
use Illuminate\Http\Request;
use App\Http\Resources\crowdfunding\BlogResource;
use App\Http\Requests\crowdfunding\BlogRequest;
use Illuminate\Routing\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BlogResource::collection(Blog::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $image = $request->file('image');
        $filename = $image->getClientOriginalName();
        $path = 'blog';
        
        $image->storeAs($path, $filename, 'public');
        
        $blog = Blog::create([
            'title'=>$request['title'],
            'description'=>$request['description'],
            'image'=>$filename,
        ]);
        
        return new BlogResource($blog);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\crowdfunding\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($blogId)
    {
        return new BlogResource(Blog::findOrFail($blogId));
    }
    
    public function random($random)
    {   
        if(is_null($random)){
            return response()->json([
                'response_code'=>'01',
                'response_message'=>'Faktor Random minimal 2, dan ini belum terisi',
            ]);
        }else{
            
            $blogs = Blog::inRandomOrder()->limit($random)->get();
            
            return BlogResource::collection($blogs);
            
        }
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\crowdfunding\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\crowdfunding\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
