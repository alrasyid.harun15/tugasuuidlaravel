<?php

namespace App\model\transaction;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Traits\UsesUuid;
use App\crowdfunding\Campaign;

class Transaction extends Model
{   
    use UsesUuid;
    
    protected $guarded = [];
    protected $table = 'transactions';
    
    public function user() {
        
        return $this->belongsTo(User::class);
        
    }
    
    public function campaigns() {
        
        return $this->belongsToMany(Campaign::class)->withPivot('amount')->withTimestamps();
        
    }
}
