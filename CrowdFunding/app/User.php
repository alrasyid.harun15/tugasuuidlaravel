<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\crowdfunding\Role;
use App\Traits\UsesUuid;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\model\article\Article;
use App\crowdfunding\OtpCode;
use App\model\chat\Chat;
use App\model\transaction\Transaction;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    protected static function boot(){
        
        parent::boot();
        
        //Default user is an Admin, cool isn't it?
        
        static::creating(function($model){
            
            $roleAdmin = Role::query()->where("name","Admin")->first();
            
            $model->role_id = $roleAdmin->id;
            
        });
        
        static::created(function($model){
            
            $model->generateOtpCode();                
                
        });
        
    }
    
    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function otpCode(){
        return $this->hasOne(OtpCode::class);
    }
    
    public function getJWTCustomClaims(){
        return [];
    }

    public function getJWTIdentifier(){
        return $this->getKey();
    }
    
    public function articles() {
        return $this->hasMany(Article::class);
    }
    
    public function generateOtpCode() {
        
        $uniqueOtp = NULL;
        $duplicationDetected=NULL;
        
        do{ 
            
            $uniqueOtp = mt_rand(100000,999999);
            $duplicationDetected = OtpCode::query()->where('otp','=',$uniqueOtp)->first();            
            
        }while(!is_null($duplicationDetected));
        
        OtpCode::create([
            'otp'=> $uniqueOtp,
            'valid_until' => new \DateTime('+5 minutes'),
            'user_id'=> $this->id
        ]);
        
    }
    
    public function chats() {
        return $this->hasMany(Chat::class);
    } 
    
    public function transactions() {
        return $this->hasMany(Transaction::class);
    } 
    
}
