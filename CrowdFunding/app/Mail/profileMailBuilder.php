<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class profileMailBuilder extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $filePath = NULL;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        
        $image = $user->image;
        
        if(!is_null($image)){
            $path = storage_path("app\public\users\\" . $user->image);
            $this->filePath = $path;
        }
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->from("no-reply@edgerun.com")
                ->subject("Informasi Profile User")         
                ->view('auth.mail.userProfileMail');
    }
}
