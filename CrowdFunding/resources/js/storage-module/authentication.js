export default{

	namespaced : true,

	state: {		
		authenticate: false,
		token: {},
		profile: {}		
	  },
	  
  	mutations: {
    	
		keepTheKey (state, attemptResult){

			state.authenticate = true

			console.log('keepTheKey : ')
			console.log(attemptResult)

			if(attemptResult){

				let {token, profile} = attemptResult

				console.log('keep the key in local : ')
				
				state.token = token
				state.profile = profile

				localStorage.setItem('TOKEN',token.access_token)

			}else

				console.log('Login cannot be commit, null attempt result')			
		},
		removeTheKey (state){

			state.authenticate = false

			console.log('removeTheKey : ')
			
			state.token = {}
			state.profile = {} 
			
		},
		checkEsixtingToken(state, suspiciousToken){

			let {token} = state

			if(token!=null){
				if(token.access_token==suspiciousToken){
					// good, everything it's ok	
					console.log('Continue process: ')

					if(suspiciousToken!=null){
						localStorage.setItem('TOKEN',suspiciousToken)
					}
				}else{
					// it seems wrong token supplied
					console.log('Who are you? Please login! ')
					state.token = {}
					state.profile = {}

				}	
			}

		}
	},
	actions:{
		
		login (state,attemptResult){

			console.log('Login : ' + attemptResult)

			if(attemptResult)

				state.commit('keepTheKey',attemptResult)
			
			else

				console.log('Login cannot be commit, null attempt result')

		},
		logout (state){

			console.log('Logout : ')

			state.commit('removeTheKey')
			
		},
		async checkToken(state, suspiciousToken){

			try {
				
				let response = await axios.post('/api/auth/checkToken',{},{headers:{Authorization: `Bearer ${suspiciousToken}`}})

				console.log(`Check suspiciousToken Initiated`)

				console.log(response)

				if(response.status==200){

					let {response_code} = response.data
										
					if(response_code==="00"){
										
						state.commit('checkEsixtingToken',suspiciousToken)
					}
				}

			} catch (err) {
				
				console.log(err)
				console.log('Token invalid, do cleareance!')
				state.commit('removeTheKey')				

			}			

		}

	},getters:{
		
		haveToken(state){

			console.log('authenticate ...?')
			
			if(state==null || state.token==null || state.token.access_token==null){
				console.log('Not authenticated')
				return null
			}else{
				console.log('authenticated')
				
				return state.token.access_token
			}
			 
		},
		whoAmI(state){
			return state.profile
		}

	}

}