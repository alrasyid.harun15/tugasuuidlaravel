<?php

namespace App\Http\Controllers\crowdfunding\profile;

use App\User;
use App\Events\ProfileRequestEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\crowdfunding\profile\ProfileRequest;
use App\Http\Resources\crowdfunding\profile\ProfileResource;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth()->user();
        return new ProfileResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request)
    {   
        
        $user = Auth()->user();
        
        $image = $request->file('image');
        $originalExt = $image->getClientOriginalExtension();
        
        $uploadFolder = 'users';
        $filename = $user->id .'.'. $originalExt;
        
        $image_uploaded_path = $image->storeAs($uploadFolder, $filename ,'public');
        
        User::query()->where('id','=', $user->id)->update([
            'image'=>$filename,
            'name'=>$request['name']
        ]);
         
        $userUpdated = User::query()->find($user->id);
        
        return new ProfileResource($userUpdated);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function sendMail()
    {   
        $user = Auth()->user();
        
        event(new ProfileRequestEvent($user));
        
        return response()->json([
            "response_code"=>"00",
            "response_message"=>"Email berhasil terkirim ke : " . $user->email ,
        ]);
    }
}
